<?php

namespace App\Controllers;

use App\Controllers\MyController;
use App\Models\MsKategoriReferensiModel;
use App\Models\MsReferensiModel;

class MsReferensi extends MyController
{
    protected $M_ms_referensi;
    public function __construct()
    {
        parent::__construct();
        $this->M_ms_referensi = new MsReferensiModel();
    }

    public function index()
    {
        $M_ms_kategori_referensi = new MsKategoriReferensiModel();
        $data['opt_refcat'] = $M_ms_kategori_referensi
            ->where('refcat_status', 1)
            ->orderBy('refcat_kode', 'asc')
            ->findAll();

        $data['title'] = "Master Referensi";
        return $this->base_theme('v_ms_referensi', $data);
    }

    public function get_data()
    {
        $columns = array(
            'reff_id',
            'reff_kode',
            'reff_nama',
            'reff_value',
            'reff_status',
            'refcat_id',
        );

        $colSearch = [
            'reff_kode',
            'reff_nama',
            'reff_value',
        ];

        $search = $this->request->getVar('search')['value'];
        $fil_refcat_id = $this->request->getVar('fil_refcat_id');

        $where = "";

        if ($fil_refcat_id) {
            $where .= " AND refcat_id = $fil_refcat_id ";
        }

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($colSearch); $i++) {
                $where .= " LOWER(" . $colSearch[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $iTotalRecords = intval($this->M_ms_referensi->get_total($where));
        $length = intval($this->request->getVar('length'));
        $length = $length < 0 ? $iTotalRecords : $length;
        $start  = intval($this->request->getVar('start'));
        $draw      = intval($_REQUEST['draw']);
        $sortCol0 = $this->request->getVar('order')[0];
        $records = array();
        $records["data"] = array();
        $order = "";
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($start) . ", " . intval($length);
        }

        if (isset($sortCol0)) {
            $order = "ORDER BY  ";
            for ($i = 0; $i < count($this->request->getVar('order')); $i++) {
                if ($this->request->getVar('columns')[intval($this->request->getVar('order')[$i]['column'])]['orderable'] == "true") {
                    $order .= "" . $columns[intval($this->request->getVar('order')[$i]['column'])] . " " .
                        ($this->request->getVar('order')[$i]['dir'] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $order = substr_replace($order, "", -2);
            if ($order == "ORDER BY") {
                $order = "";
            }
        }
        $data = $this->M_ms_referensi->get_data($limit, $where, $order, $columns);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $isi = rawurlencode(json_encode($row));

            if ($row->reff_status == 1) {
                $status = '<span class="badge rounded-pill badge-success">Aktif</span>';
            } else {
                $status = '<span class="badge rounded-pill badge-danger">Non Aktif</span>';
            }

            $action .= '<div class="d-grid gap-2 d-md-block">
                            <button onclick="set_val(\'' . $isi . '\')" class="btn btn-sm btn-primary" title="Edit">
                                <i class="fa fa-pencil-alt"></i>
                            </button>
                            <button onclick="set_del(\'' . $row->reff_id . '\')" class="btn btn-sm btn-danger " title="Delete">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>';

            $records["data"][] = array(
                $no++,
                $row->reff_kode,
                $row->reff_nama,
                $row->reff_value,
                $status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function save()
    {
        $act = addslashes($this->request->getVar('act'));
        $data = [
            'reff_kode' => addslashes($this->request->getVar('reff_kode')),
            'reff_nama' => addslashes($this->request->getVar('reff_nama')),
            'reff_value' => addslashes($this->request->getVar('reff_value')),
            'reff_status' => $this->request->getVar('reff_status'),
            'refcat_id' => $this->request->getVar('refcat_id'),
        ];

        if ($act == 'add') {
            $res = $this->M_ms_referensi->insert($data);
        } else {
            $id = $this->request->getVar('reff_id');
            $res = $this->M_ms_referensi->update($id, $data);
        }

        if ($res > 0) {
            $response = [
                'status' => true,
                'message' => $act == 'add' ? 'Berhasil menambahkan data!' : 'Berhasil memperbarui data!',
                'title' => 'Success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' =>  $act == 'add' ? 'Gagal menambahkan data!' : 'Gagal memperbarui data!',
                'title' => 'Error',
            ];
        }

        echo json_encode($response);
    }

    public function hapus($id)
    {
        $res = $this->M_ms_referensi->delete($id);

        $response = [
            'status' => false,
            'message' => "Data Gagal dihapus"
        ];

        if ($res) {
            $response = [
                'status' => true,
                'message' => "Data Berhasil dihapus"
            ];
        }

        echo json_encode($response);
    }
}
