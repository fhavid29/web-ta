<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MsKategoriReferensiSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'refcat_id'     => 1,
                'refcat_kode'   => '01',
                'refcat_nama'   => 'Kondisi Fuzzy',
                'refcat_status' => 1,
                'refcat_desc'   => '',
            ],
            [
                'refcat_id'     => 2,
                'refcat_kode'   => '02',
                'refcat_nama'   => 'Keterangan Gangguan',
                'refcat_status' => 1,
                'refcat_desc'   => '',
            ],
        ];

        $this->db->table("ms_refcat")->insertBatch($data);
    }
}
