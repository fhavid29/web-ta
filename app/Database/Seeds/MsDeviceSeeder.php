<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MsDeviceSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'device_kode' => "01",
                'device_nama' => "Device 1",
                'device_status' => 1,
            ],
        ];

        $this->db->table("ms_device")->insertBatch($data);
    }
}
