<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ListDeviceSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'user_id' => 1,
                'device_id' => 1,
                'ld_kode' => "asadadadaf",
                'ld_url' => "http://192.168.1.14",
                'ld_status' => 1,
            ], [
                'user_id' => 2,
                'device_id' => 1,
                'ld_kode' => "abcdef",
                'ld_url' => "http://192.168.1.15",
                'ld_status' => 1,
            ],
        ];

        $this->db->table("list_device")->insertBatch($data);
    }
}
