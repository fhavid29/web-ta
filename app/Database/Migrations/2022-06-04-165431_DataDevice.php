<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class DataDevice extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'dd_id'          => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'ld_id'          => [
                'type'           => 'INT',
                'unsigned'       => true,
            ],
            'v1'          => [
                'type'           => 'float',
                'constraint'    => '10,2',
                'null' => true,
            ],
            'v2'          => [
                'type'           => 'float',
                'constraint'    => '10,2',
                'null' => true,
            ],
            'v3'          => [
                'type'           => 'float',
                'constraint'    => '10,2',
                'null' => true,
            ],
            'i1'          => [
                'type'           => 'float',
                'constraint'    => '10,2',
                'null' => true,
            ],
            'i2'          => [
                'type'           => 'float',
                'constraint'    => '10,2',
                'null' => true,
            ],
            'i3'          => [
                'type'           => 'float',
                'constraint'    => '10,2',
                'null' => true,
            ],
            'kwh'          => [
                'type'           => 'float',
                'constraint'    => '10,2',
                'null' => true,
            ],
            'timer'          => [
                'type'           => 'float',
                'constraint'    => '10,2',
                'null' => true,
            ],
            'output'            => [
                'type'          => 'int',
                'constraint'    => '11',
                'null' => true,
            ],
            'gangguan'          => [
                'type'           => 'text',
                'null' => true,
            ],
            'created_at datetime default current_timestamp',
        ]);
        $this->forge->addKey('dd_id', true);
        $attributes = ['ENGINE' => 'InnoDB'];
        $this->forge->createTable('data_device', false, $attributes);
    }

    public function down()
    {
        $this->forge->dropTable('data_device');
    }
}
