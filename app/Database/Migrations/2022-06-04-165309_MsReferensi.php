<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MsReferensi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'reff_id'             => [
                'type'              => 'INT',
                'unsigned'          => true,
                'auto_increment'    => true,
            ],
            'reff_kode'           => [
                'type'              => 'varchar',
                'constraint'        => '255',
                'null'              => true,
            ],
            'reff_nama'           => [
                'type'              => 'varchar',
                'constraint'        => '255',
                'null'              => true,
            ],
            'reff_status'         => [
                'type'              => 'INT',
                'constraint'        => '1',
                'default'           => 0,
            ],
            'reff_value'           => [
                'type'              => 'text',
            ],
            'refcat_id'             => [
                'type'              => 'int',
                'unsigned'          => true,
            ],
        ]);
        $this->forge->addKey('reff_id', true);
        $this->forge->addForeignKey('refcat_id', 'ms_refcat', 'refcat_id', 'CASCADE', 'CASCADE');
        $attributes = ['ENGINE' => 'InnoDB'];
        $this->forge->createTable('ms_reff', false, $attributes);
    }

    public function down()
    {
        $this->forge->dropTable('ms_reff');
    }
}
