<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MsKategoriReferensi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'refcat_id'             => [
                'type'              => 'INT',
                'unsigned'          => true,
                'auto_increment'    => true,
            ],
            'refcat_kode'           => [
                'type'              => 'varchar',
                'constraint'        => '255',
                'null'              => true,
            ],
            'refcat_nama'           => [
                'type'              => 'varchar',
                'constraint'        => '255',
                'null'              => true,
            ],
            'refcat_status'         => [
                'type'              => 'INT',
                'constraint'        => '1',
                'default'           => 0,
            ],
            'refcat_desc'           => [
                'type'              => 'text',
            ],
        ]);
        $this->forge->addKey('refcat_id', true);
        $attributes = ['ENGINE' => 'InnoDB'];
        $this->forge->createTable('ms_refcat', false, $attributes);
    }

    public function down()
    {
        $this->forge->dropTable('ms_refcat');
    }
}
