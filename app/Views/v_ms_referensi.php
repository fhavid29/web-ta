<h1 class="mt-4"><?= $title ?></h1>
<div class="row" id="rowForm" style="display: none;">
  <div class="col-xl-12">
    <div class="card mb-4">
      <div class="card-header">
        <div class="row">
          <div class="col-sm-6 align-self-center">
            <h4 class="card-title mb-0">
              Form <?= $title ?>
            </h4>
          </div>
          <div class="col-sm-6">
            <button class="btn btn-primary float-right" type="button" onclick="$('#btnCancel').click()"><i class="fa fa-database"></i> List</button>
          </div>
        </div>
      </div>
      <div class="card-body">
        <!--begin::Form-->
        <form class="form" id="form_vendor">
          <input type="hidden" name="act" id="act" value="add">
          <input type="hidden" name="reff_id" id="reff_id">
          <div class="row mb-3">
            <label class="col-md-3 col-form-label">Kategori</label>
            <div class="col-md-5">
              <select class="form-control" id="refcat_id" name="refcat_id">
                <?php foreach ($opt_refcat as $v) : ?>
                  <option value="<?= $v->refcat_id ?>"><?= "$v->refcat_kode - $v->refcat_nama" ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-md-3 col-form-label">Kode</label>
            <div class="col-md-5">
              <input type="text" class="form-control" placeholder="Kode" id="reff_kode" name="reff_kode" />
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-md-3 col-form-label">Nama</label>
            <div class="col-md-5">
              <input type="text" class="form-control" placeholder="Nama" id="reff_nama" name="reff_nama" />
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-md-3 col-form-label">Nilai</label>
            <div class="col-md-5">
              <input type="text" class="form-control" placeholder="Nilai" id="reff_value" name="reff_value">
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-md-3 col-form-label">Status</label>
            <div class="col-md-3">
              <select class="form-control" id="reff_status" name="reff_status">
                <option value="1">Aktif</option>
                <option value="0">Non Aktif</option>
              </select>
            </div>
          </div>
        </form>
        <!--end::Form-->
      </div>
      <div class="card-footer text-center">
        <div class="d-grid gap-2 d-md-block">
          <button type="button" id="btnSave" class="btn btn-primary">Save</button>
          <button type="button" id="btnCancel" class="btn btn-secondary">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row" id="rowList">
  <div class="col-xl-12">
    <div class="card mb-4">
      <div class="card-header">
        <div class="row">
          <div class="col-sm-6 align-self-center">
            <h4 class="card-title">
              Data <?= $title ?>
            </h4>
          </div>
          <div class="col-sm-6">
            <button type="button" id="btnAdd" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Data</button>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <label class="control-label col-md-3">Kategori</label>
              <div class="col-md-9">
                <select name="fil_refcat_id" id="fil_refcat_id" class="form-control">
                  <?php foreach ($opt_refcat as $v) : ?>
                    <option value="<?= $v->refcat_id ?>"><?= "$v->refcat_kode - $v->refcat_nama" ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <!--begin::DataTable-->
        <table class="table table-bordered table-hover table-checkable" id="tbl_vendor" style="margin-top: 13px !important; width: 100%;">
          <thead>
            <tr>
              <th class="text-center">No.</th>
              <th class="text-center">Kode</th>
              <th class="text-center">Nama</th>
              <th class="text-center">Nilai</th>
              <th class="text-center">Status</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
        </table>
        <!--end::DataTable-->
      </div>
    </div>
  </div>
</div>

<script>
  var TableAdvanced = function() {

    var initTable1 = function() {
      var table = $('#tbl_vendor');

      // begin first table
      table.DataTable({
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        ajax: {
          url: '<?= base_url() ?>/ms-reff/get-data',
          type: 'POST',
          data: function(d) {
            d.fil_refcat_id = $("#fil_refcat_id").val();
          },
        },
        columnDefs: [{
          targets: [0, -1],
          orderable: false,
        }, {
          targets: [0, -2, -1],
          className: 'text-center',
        }, ],
        "order": [
          [1, 'asc']
        ]
      });
    };

    var initForm = function() {
      const btnSave = $('#btnSave');
      const formVendor = $("#form_vendor")

      btnSave.click(function() {
        formVendor.submit();
      })

      // Validation Rules
      formVendor.validate({
        rules: {
          reff_nama: {
            required: true,
          },
          reff_kode: {
            required: true,
          },
          reff_value: {
            required: true,
          },
        },
        errorClass: 'help-block',
        errorElement: 'span',
        ignore: 'input[type=hidden]',
        highlight: function(el, errorClass) {
          $(el).closest('.row').first().addClass('has-error');
        },
        unhighlight: function(el, errorClass) {
          var $parent = $(el).closest('.row').first();
          $parent.removeClass('has-error');
          $parent.find('.help-block').hide();
        },
        errorPlacement: function(error, el) {
          error.appendTo(el.closest('.row').find('div:first'));
        },
        submitHandler: function(form) {
          btnSave.attr('disabled', 'disabled').text('Loading...');
          let data = formVendor.serialize()

          $.ajax({
            url: '<?= base_url() ?>/ms-reff',
            data: data,
            type: 'post',
            dataType: 'json',
            complete: function() {
              btnSave.removeAttr('disabled', 'disabled').text('Save');
            },
            error: function() {
              btnSave.removeAttr('disabled', 'disabled').text('Save');
            },
            success: res => {
              if (res.status) {
                $("#btnCancel").click()
                Swal.fire({
                  icon: "success",
                  title: "Success",
                  html: res.message,
                  showConfirmButton: false,
                  timer: 1500
                })
              } else {
                Swal.fire({
                  icon: "error",
                  title: "Error",
                  html: res.message,
                })
              }
            }
          })
        }
      });
    }

    return {

      //main function to initiate the module
      init: function() {
        initTable1();
        initForm();
      },

    };

  }();

  function resetForm() {
    $("#form_vendor")[0].reset()
    $("#form_vendor").validate().resetForm()
    $(".has-error").removeClass('has-error')
    $("#act").val('add')
    $("#reff_id").val('')
    $('#btnSave').removeAttr('disabled', 'disabled').text('Save');
  }

  function reload_tbl() {
    $("#tbl_vendor").dataTable().fnDraw();
  }

  function set_val(data) {
    const isi = JSON.parse(decodeURIComponent(data))

    $("#act").val('edit')
    $("#reff_id").val(isi.reff_id)
    $("#reff_nama").val(isi.reff_nama)
    $("#reff_kode").val(isi.reff_kode)
    $("#reff_status").val(isi.reff_status)
    $("#reff_value").val(isi.reff_value)
    $("#refcat_id").val(isi.refcat_id)
    $("#rowForm").slideDown(500)
    $("#rowList").slideUp(500)

  }

  function set_del(id) {
    Swal.fire({
      title: "Apakah Anda yakin?",
      text: "Data yang dihapus tidak dapat dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Iya",
      cancelButtonText: "Tidak",
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>/ms-reff/' + id,
          method: 'delete',
          dataType: 'json',
          cache: false,
          success: res => {
            if (res.status) {
              Swal.fire({
                icon: "success",
                title: "Success",
                html: res.message,
                showConfirmButton: false,
                timer: 1500
              })
              reload_tbl()
            } else {
              Swal.fire({
                icon: "error",
                title: "Error",
                html: res.message,
              })
            }
          }
        })
      }
    });
  }

  jQuery(document).ready(function() {
    TableAdvanced.init();

    $("#btnCancel").click(function() {
      resetForm()
      reload_tbl()
      $("#rowForm").slideUp(500)
      $("#rowList").slideDown(500)
    })

    $("#btnAdd").click(function() {
      resetForm()
      $("#rowForm").slideDown(500)
      $("#rowList").slideUp(500)
    })

    $("#fil_refcat_id").change(function() {
      reload_tbl()
    })
  });
</script>